package com.robotemplates.cityguide;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.aspsine.multithreaddownload.CallBack;
import com.aspsine.multithreaddownload.DownloadException;
import com.aspsine.multithreaddownload.DownloadManager;
import com.aspsine.multithreaddownload.DownloadRequest;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.robotemplates.cityguide.activity.MainActivity;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Ahmad.nemati on 4/9/2018.
 */

public class SplashActivity extends AppCompatActivity implements PermissionListener {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (Build.VERSION.SDK_INT >= 23) {

            TedPermission.with(this)
                    .setPermissionListener(this)
                    .setDeniedMessage("لطفا همه ی پرمیشن ها را تایید کنید!!!")
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION)
                    .check();

        } else {
            makeRequest();
        }

    }

    @Override
    public void onPermissionGranted() {
        makeRequest();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermissions) {

    }

    private void makeRequest() {
        Log.e("Per", "Acc");

        DownloadManager.getInstance().download(getDownloadRequest(), "t", new CallBack() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onConnecting() {

            }

            @Override
            public void onConnected(long total, boolean isRangeSupport) {

            }

            @Override
            public void onProgress(long finished, long total, int progress) {

            }

            @Override
            public void onCompleted() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onDownloadPaused() {

            }

            @Override
            public void onDownloadCanceled() {

            }

            @Override
            public void onFailed(DownloadException e) {

            }
        });
    }

    private DownloadRequest getDownloadRequest() {
        File file = new File("/data/data/" + CityGuideApplication.getContext().getPackageName() + "/databases/");
        if (file.exists()) {
            file.delete();
            file.mkdir();
        }


        return new DownloadRequest.Builder()
                .setName("shopfinder.db")
                .setUri("http://anishtan.com/shopfinder.db")
                .setFolder(file)
                .build();
    }

}
